package main

import "fmt"

type LinkList struct {
	Value interface{}
	Next  *LinkList
}

func main() {
	fmt.Println("判断是否为空：")
	head := Init(0)
	fmt.Println(head)
	head.Insert(1)
	fmt.Println(head.Next, head)
	head.Insert(2)
	fmt.Println(head, head.Next, head.Next.Next)
	head.Insert(3)
	fmt.Println(head, head.Next, head.Next.Next, head.Next.Next.Next)
	head.Traverse();
	fmt.Println(head.Get(3))
	head.Delete(2)
	head.Traverse()
}

func Init(value interface{}) *LinkList {
	link := new(LinkList)
	link.Value = value
	link.Next = nil
	return link
}

func (pLink *LinkList) IsEmpty() bool {
	if pLink == nil {
		return true
	}

	return false
}

func (pLink *LinkList) Insert(v interface{}) {
	head := pLink
	for {
		if pLink.Next != nil {
			pLink = pLink.Next
		} else {
			last := pLink
			temp := &LinkList{
				v,
				nil,
			}
			last.Next = temp
			pLink = head
			break
		}
	}
}

func (pLink *LinkList) Traverse() {
	head := pLink
	for head != nil {
		fmt.Println(head.Value)
		head = head.Next
	}

	fmt.Println("=========end=========")
}

//获取第i个元素
func (pLink *LinkList) Get(i int) interface{} {
	head := pLink
	for j := 1; j < i; j++ {
		head = head.Next
	}
	return head.Value
}

//删除第i个元素，默认删除表头元素
func (pLink *LinkList) Delete(i int) {
	current := pLink
	for j := 1; j < i; j++ {
		if j == (i - 1) {
			current.Next = current.Next.Next
			break
		} else {
			current = current.Next
		}

	}
}
